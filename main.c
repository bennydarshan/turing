#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

#include <string.h>

#define BUFF_SIZE 64

typedef enum {MOVE_LEFT = -1, MOVE_RIGHT = 1, UNDEFINED = 0} move;



typedef struct{
	char x;
	char q;
}state;

typedef struct{
	char x;
	char q;
	move m;
	bool term;

	
}instruct;

typedef struct{
	state s;
	instruct inst;
}entry;

typedef struct{
	char x;
	int i;
	entry tb[BUFF_SIZE];
}machine;

//@ x
//$ ribbon
//* match on anything
//; comment
//_ empty

bool legal_char(char ch, bool is_state)
{
	if((ch >= 'a') && (ch <= 'z')) return true;
	if((ch >= 'A') && (ch <= 'Z')) return true;
	switch(ch) {
	case '@':
	case '$': if(is_state) return false;
	case '*':
	case '_': return true;
	default: return false;
	}

}

bool state_cmp(state *this, state *s)
{
	if((this->x != '*') && (s->x != '*') && (this->x != s->x)) return false;
	if((this->q != '*') && (s->q != '*') && (this->q != s->q)) return false;
	return true;
}

void trim(char **self)
{
	while((**self == ' ') || (**self == '\t')) (*self)++;
}

state state_init()
{
	return (state){'\0', '\0'};
}

instruct instruct_init()
{
	return (instruct){'\0', '\0', UNDEFINED, false};
}

entry entry_init()
{
	return (entry){state_init(), instruct_init()};
}

bool preprocess(char *line, char *ret)
{
	memcpy(ret, &"\0\0\0\0\0\0\0\0", 8);
	char *iter = line;
	for(int i=0;i<8;++i) {
		if((i<7) && ((*iter == '\0') || (*iter == '\n'))) {
			printf("FATAL: illegal NEWLINE or NULLTERMINATOR\n");
			return false;
		}
		trim(&iter);
		ret[i] = *iter;++iter;

	}
	return true;

}


bool entry_read(char *line, entry *ret)
{
	*ret = entry_init();
	char arr[8];
	preprocess(line, arr);
	if(!legal_char(arr[0], true)) {
		printf("FATAL: ILEGAL CHAR in state register\n");
		return false;
	}
	ret->s.x = arr[0];
	if(!legal_char(arr[1], true)) {
		printf("FATAL: ILEGAL CHAR in state ribbon\n");
		return false;
	}

	ret->s.q = arr[1];
	if((arr[2] != '-') || (arr[3] != '>')) {
		printf("FATAL: arrow missing!\n");
		return false;
	}
	if(!legal_char(arr[4], false)) {
		printf("FATAL: ILEGAL CHAR in instruction register\n");
		return false;
	}
	switch(arr[4]) {
	case '@': ret->inst.x = ret->s.x; break;
	case '$': ret->inst.x = ret->s.q; break;
	default: ret->inst.x = arr[4]; break;
	}
	if(!legal_char(arr[5], false)) {
		printf("FATAL: ILEGAL CHAR in instruction ribbon\n");
		return false;
	}

	switch(arr[5]) {
	case '@': ret->inst.q = ret->s.x;break;
	case '$': ret->inst.q = ret->s.q; break;
	default: ret->inst.q = arr[5]; break;
	}
	if(arr[6] == 'L') ret->inst.m = MOVE_LEFT;
	else if(arr[6] == 'R') ret->inst.m = MOVE_RIGHT;
	else {printf("FATAL: move instruction incorrect\n");return false;}
	ret->inst.term = (arr[7] == 'B');
	return true;



}

bool entries_fromfile(entry *entries, FILE *fp)
{
	assert(fp);
	assert(entries);
	char buff[BUFF_SIZE];
	int i = 0;
	while(fgets(buff, BUFF_SIZE, fp)) {
		if((buff[0] == ';') || (buff[0] == '\n')) continue;
		if(!entry_read(buff, entries + i)) return false;
		++i;
	}
	return true;

}


int next(machine *m, char memory[])
{
	state tmp = {m->x, memory[m->i]};
	instruct inst = instruct_init();
	for(int i = 0;m->tb[i].s.x != '\0'; ++i) {
		if(state_cmp(&tmp, &m->tb[i].s)) {
			inst = m->tb[i].inst;
			break;
		}
	}
	if(inst.x == '\0') {
		printf("FATAL: entry not found!\nfor state: {register: %c, ribbon: %c}", tmp.x, tmp.q);
		assert(0);
	}
	
	memory[m->i] = inst.q;
	m->x = inst.x;
	switch(inst.m) {
	case MOVE_LEFT:
		assert(m->i > 0);
		m->i -= 1;
		break;
	case MOVE_RIGHT:
		assert(m->i < BUFF_SIZE - 1);
		m->i += 1;
		break;
	default:
		assert(0);
	}
	if(inst.term) return 1;
	else return 0;


}

int main(int argc, char *argv[])
{
	if(argc < 3) {
		printf("usage: %s <instructions> <ribbon>\n", argv[0]);
		return 1;
	}
	machine m = {'_', BUFF_SIZE/2, {entry_init()}};
	FILE *fp = fopen(argv[1], "r");
	assert(fp);
	assert(entries_fromfile(m.tb, fp));
	fclose(fp);
	char memory[BUFF_SIZE] = {'\0'};
	fp = fopen(argv[2], "rb");
	assert(fp);
	int memory_size = fread(memory + BUFF_SIZE/2, 1, BUFF_SIZE/2, fp);
	fclose(fp);

	while(true) {
		if(next(&m, memory) == 1) break;
	}
	fp = fopen("memory.out", "wb");
	fwrite(memory, 1, BUFF_SIZE, fp);





}
